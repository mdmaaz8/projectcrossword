#include<iostream>
#include<map>
#include<vector>
#include<string>
#include <fstream>
#include "headerFile.h"
#define N 20
using namespace std;
map<int, vector<string> > genMap();
vector<pair<int,char> > get_char_pos();
vector<string> returnValidWords( map<int, vector<string> > dict, vector< pair<int, char> > pos, int length );
void printCharMatrix(vector<vector<char> >, int);
int main() {
        int xo;
        vector<string> usedWords;
        map<int, vector<string> > dict = genMap();
        //cout << "here" << dict[4][3];
		vector<string> crossWord = getInput();
		int numberMatrix[N][N] = {0};
		doNumbering(crossWord, numberMatrix);
		printNumberMatrix(numberMatrix, crossWord.size());
		map<int, PAIR_II> wordsLength = calcWordsLength(numberMatrix, crossWord.size());
		map<int, PAIR_II> :: iterator it;
		for(it = wordsLength.begin(); it != wordsLength.end(); it++) {
				cout << (*it).first << ":" << (*it).second.first << " " << (*it).second.second << endl;
		}
		map<PAIR_IC, vector<int> > dependencyList;
		dependencyList = createDependencyList(numberMatrix, crossWord.size());
		map<PAIR_IC, vector<int> > :: iterator is;

		for(is = dependencyList.begin(); is != dependencyList.end(); is++) {
				cout << (*is).first.first << (*is).first.second << ":";
				for(int i = 0; i < (*is).second.size(); i++) {
						cout << (*is).second[i] << " " ;
				}
				cout << endl;

		}
		vector<vector<char> > charMatrix = generateCharMatrix(numberMatrix, crossWord.size());
		printCharMatrix(charMatrix, crossWord.size());

		for (int i = 1; i < crossWord.size()-1; i++) {
			for(int j =1; j < crossWord.size()-1; j++) {
				
	//			cout<<"|||||||||||||||||||||||||||||||||"<<endl;
				if (numberMatrix[i][j] > 0){
                    int wordId = numberMatrix[i][j];
					if (numberMatrix[i][j-1] == -1 && numberMatrix[i][j+1] != -1){
                       
                       PAIR_IC id = make_pair(wordId,'a');
                       vector <PAIR_IC> wordConstraint = returnWordConstraint(charMatrix,id,numberMatrix);
                       //cout << wordsLength[wordId].first<<endl;
                       vector<string> validWords= returnValidWords(dict, wordConstraint, wordsLength[wordId].first);
                        if (validWords.size() > 0){
						    if(usedWords.size()>0){
							    for (int iInVw = 0; iInVw < validWords.size(); iInVw++){                             
								    if(find (usedWords.begin(), usedWords.end(), validWords[iInVw])== usedWords.end() && validWords[iInVw] != usedWords[usedWords.size()-1]){
									   charMatrix = writeToCharMatrix(charMatrix, i, j,validWords[iInVw] , 'a', wordsLength[wordId].first);
									   usedWords.push_back(validWords[iInVw]);
									   //cout << validWords[0] << wordsLength[wordId].first;
							//	       cin >> xo;
                                       break;
								    }
							    }
						    }
							else{
								cout << validWords[0] << wordsLength[wordId].first;
							//	cin >> xo;
								charMatrix = writeToCharMatrix(charMatrix, i, j,validWords[0] , 'a',wordsLength[wordId].first);
								usedWords.push_back(validWords[0]);
							}
						}
						else{
                                cout << "Need to do back tracking" << endl;
                             }
						printCharMatrix(charMatrix,crossWord.size());
						cout<<endl;
					}
					
					if (numberMatrix[i-1][j] == -1 && numberMatrix[i+1][j] != -1){
                       //	cout<<"dddddddddddddddddddddddddddddd"<<endl;
                        PAIR_IC id = make_pair(wordId,'d');
                        vector <PAIR_IC> wordConstraint = returnWordConstraint(charMatrix,id,numberMatrix);
                        // cout << wordsLength[wordId].first<<endl;
                        vector<string> validWords = returnValidWords(dict, wordConstraint, wordsLength[wordId].second);
                        //cout << validWords[0] << endl;
                        if (validWords.size() > 0){
							if (usedWords.size() >0){
								for (int iInVw = 0; iInVw < validWords.size(); iInVw++){                             
									if(find (usedWords.begin(), usedWords.end(), validWords[iInVw])== usedWords.end() && validWords[iInVw] != usedWords[usedWords.size()-1]){
									   charMatrix = writeToCharMatrix(charMatrix, i, j,validWords[iInVw] , 'd',wordsLength[wordId].second);
									   cout << validWords[iInVw] <<wordsLength[wordId].second<<endl;
									   usedWords.push_back(validWords[iInVw]);
									   cout << validWords[0] <<wordsLength[wordId].second<<endl;
								  //     cin >> xo;
                                       break;
									}
								}
							}
						    else{
								cout << validWords[0] <<wordsLength[wordId].second<<endl;
								//cin >> xo;
								charMatrix = writeToCharMatrix(charMatrix, i, j,validWords[0] , 'd',wordsLength[wordId].second);
								usedWords.push_back(validWords[0]);
						    }
						   // cout <<"checked used Words" << endl;
					    }
					    else{
                                cout << "Need to do back tracking" << endl;
                             }
						printCharMatrix(charMatrix,crossWord.size());
						cout<<endl;
					}
				//	cout << "************************";
					
				}
			}
   }
        //int xo;
        //cout << "Before the return";
      //  cin >> xo;
		return 0;
}



/*
int main() {
    map<int, vector<string> > dict = genMap();
    vector<pair<int, char> > char_pos = get_char_pos();
    vector<string> words = returnValidWords( dict, char_pos, 3);
    for ( int i = 0; i < words.size(); i++ )
        cout << words[i] << endl;
    return 0;
}
*/

map<int,vector<string> >genMap(){
	
	map<int,vector<string> >m;
  	string line;
  	ifstream myfile("../data_source/refined.txt");
    
  	if (myfile.is_open())
  	{
    	while ( myfile.good() )
    	{
      		getline (myfile,line);
			m[line.size()].push_back(line);
 	  	}
    	myfile.close();
  	}
	else
		cout << "Unable to open file. Check path of refined.txt"; 
	return m;
}

vector<pair<int,char> > get_char_pos() {
    vector<pair<int, char> > char_pos;
    char_pos.push_back(make_pair(0,'a'));
    char_pos.push_back(make_pair(2,'k'));
    return char_pos;
}

vector<string> returnValidWords( map<int, vector<string> > dict, vector< pair<int, char> > pos, int length ) {
    
    if (pos.empty()){
        return dict.find(length)->second;
        }
    vector<string> checkWords = dict.find(length)->second;
    vector<string> validWords;
    bool valid = true;
    for ( int i = 0; i < checkWords.size(); i++ ) {
        for (int j = 0; j < pos.size(); j++) {
            if (checkWords[i][pos[j].first] != pos[j].second)
                valid = false;
        }
        if (valid)
            validWords.push_back(checkWords[i]);
        valid = true;
    }
	return validWords;
}
    
void printCharMatrix(vector<vector<char> > charMatrix, int length){
     for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						cout << charMatrix[i][j] << " ";
				}
				cout << endl;
		}
     
     
}
