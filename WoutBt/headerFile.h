#include<iostream>
#include<map>
#include<vector>
#include<string>
#define N 20

using namespace std;
typedef pair<int,int> PAIR_II;
typedef pair<int,char> PAIR_IC;

//function declarations
void doNumbering(vector<string> crossWord, int numberMatrix[N][N]);
bool isWhite(char ch);
bool isWordStartInRow(int index, int rowFlag, string rowString);
bool isWordStartInCol(int index, int colFlag, string nextRowString);
vector<string> getInput();
map<int,PAIR_II> calcWordsLength(int numberMatrix[N][N], int length);
int getWordLengthAcross(int i, int j, int numberMatrix[N][N]);
int getWordLengthDown(int i, int j, int numberMatrix[N][N]);
vector<vector<char> > generateCharMatrix(int numberMatrix[N][N], int length);
vector <PAIR_IC> returnWordConstraint(vector<vector<char> > charMatrix, PAIR_IC p, int numberMatrix);

//function implementations
vector<vector<char> > generateCharMatrix(int numberMatrix[N][N], int length) {
		vector<vector<char> > charMatrix(length, vector<char>(length));
		for(int i = 0; i < length; i++) {
				charMatrix[0][i] = '#';
				charMatrix[i][0] = '#';
				charMatrix[length-1][i] = '#';
				charMatrix[i][length-1] = '#';
		}
		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(numberMatrix[i][j] == -1)
								charMatrix[i][j] = '#';
						if(numberMatrix[i][j] >= 0)
								charMatrix[i][j] = '-';
				}
		}
		return charMatrix;
}
vector<vector<char> > writeToCharMatrix(vector<vector<char> > charMatrix, int x, int y, string str, char direction, int length) {
        if (direction == 'a'){
           int id = 0;
           for ( int i = y; i < y+length; i++){
               charMatrix[x][i] = str[id];
               id++;
           } 
        }
        if (direction == 'd'){
           int id = 0;
           for ( int i = x; i < x+length; i++){
               charMatrix[i][y] = str[id];
               id++;
           } 
        }                

        return charMatrix;
}
vector <PAIR_IC> returnWordConstraint(vector<vector<char> > charMatrix, PAIR_IC p, int numberMatrix[N][N]) {
		int length = charMatrix.size();
		vector <PAIR_IC> wordConstraints;

		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(numberMatrix[i][j] == p.first) {
								if(p.second == 'a'){
										int index = 0;
										int temp = j;
										while(charMatrix[i][temp] != '#'){
												if (charMatrix[i][temp] != '-')
														wordConstraints.push_back(make_pair(index,charMatrix[i][temp]));
												temp++;
												index++;
										}
								}
								else {
										int index = 0;
										int temp = i;
										while(charMatrix[temp][j] != '#'){
												if (charMatrix[temp][j] != '-')
														wordConstraints.push_back(make_pair(index,charMatrix[temp][j]));
												temp++;
												index++;
										}
								}	
								return wordConstraints;
						}

				}
		}

}



map<int,PAIR_II> calcWordsLength(int numberMatrix[N][N], int length) {
		map<int, PAIR_II> wordsLength;
		int across, down;
		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(numberMatrix[i][j] > 0) {
							if(numberMatrix[i][j-1] == -1)			
								across = getWordLengthAcross(i, j, numberMatrix);
							else
								across = 0;
							if(numberMatrix[i-1][j] == -1)
								down = getWordLengthDown(i, j, numberMatrix);
							else 
								down = 0;
							wordsLength[numberMatrix[i][j]] = make_pair(across,down);
						}
				}
		}
		return wordsLength;
}

map<PAIR_IC, vector<int> > createDependencyList(int numberMatrix[N][N], int length ){
		map<PAIR_IC, vector<int> > dependencyList;
		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(numberMatrix[i][j] > 0) {
								if(numberMatrix[i][j-1] == -1){
									int l = j;
									vector<int> list;

									list.push_back(-1);
									while(numberMatrix[i][l] != -1){
											int k = i;
											if(numberMatrix[i-1][l] != -1){
												while(numberMatrix[k-1][l] != -1) {
													k--;
												}
												list.push_back(numberMatrix[k][l]);
											}
											l++;
									}

									sort(list.begin(), list.end());
									reverse(list.begin(), list.end());
									dependencyList[make_pair(numberMatrix[i][j], 'a')] = list;
								}
								if(numberMatrix[i-1][j] == -1){
										int l = i;
										vector<int> list;
										list.push_back(-1);
										int k = j;
										if(numberMatrix[l][k-1] != -1){
												while(numberMatrix[l][k-1] != -1) {
														k--;
												}
												list.push_back(numberMatrix[l][k]);
										}
										sort(list.begin(), list.end());
										reverse(list.begin(), list.end());
										dependencyList[make_pair(numberMatrix[i][j],'d')] = list;
								}
						}
				}
		}
		return dependencyList;
}
int getWordLengthAcross(int i, int j, int numberMatrix[N][N]) {
		int wordLength = 0;
		if (numberMatrix[i][j+1] !=-1){
           while(numberMatrix[i][j] != -1) {
				wordLength++;
				j++;
		  }
       }
		return wordLength;
}

int getWordLengthDown(int i, int j, int numberMatrix[N][N]) {
		int wordLength = 0;
		if (numberMatrix[i+1][j] !=-1){
		   while(numberMatrix[i][j] != -1) {
				wordLength++;
				i++;
		   }
        }
		
		return wordLength;
}

void doNumbering(vector<string> crossWord, int numberMatrix[N][N]) {
		int length = crossWord.size();
		int rowFlag[N] = {0};
		int colFlag[N] = {0};
		int counter = 1;
		for(int i = 0; i < length; i++) {
				numberMatrix[0][i] = -1;
				numberMatrix[i][0] = -1;
				numberMatrix[length-1][i] = -1;
				numberMatrix[i][length-1] = -1;
		}
		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(isWhite(crossWord[i][j])) {
								if(isWordStartInRow(j, rowFlag[i], crossWord[i]) || isWordStartInCol(j, colFlag[j], crossWord[i+1])) {
										numberMatrix[i][j] = counter++;
										rowFlag[i] = 1;
										colFlag[j] = 1;
								}
						}
						else {
								numberMatrix[i][j] = -1;
								rowFlag[i] = 0;
								colFlag[j] = 0;
						}
				}
		}
}

bool isWhite(char ch) {
		return ch == 'W';
}

bool isWordStartInRow(int index, int rowFlag, string rowString) {
		return rowFlag == 0 && isWhite(rowString[index+1]);
}

bool isWordStartInCol(int index, int colFlag, string nextRowString) {
		return colFlag == 0 && isWhite(nextRowString[index]);
}

void printNumberMatrix(int numberMatrix[N][N], int length) {
		for(int i = 0; i < length-1; i++) {
				for(int j = 0; j < length-1; j++) {
						if(numberMatrix[i][j] == 0)
								cout << "__  ";
						else if(numberMatrix[i][j] == -1)
								cout << "##  ";
						else {
								if(numberMatrix[i][j] < 10)
										cout << " " << numberMatrix[i][j] << "  ";
								else
										cout << numberMatrix[i][j] << "  ";
						}
				}
				cout << endl;
		}
}

vector<string> getInput() {
		vector<string> crossWord;
		crossWord.push_back("###############");
		crossWord.push_back("#WWWWW#WWWWWWW#");
		crossWord.push_back("#W#W#W#W#W#W#W#");
		crossWord.push_back("#WWW#WWWWWWWWW#");
		crossWord.push_back("#W#W#W#W#W###W#");
		crossWord.push_back("#WWWWWWW#WWWWW#");
		crossWord.push_back("#W#W###W#W#W###");
		crossWord.push_back("#WWWWWW#WWWWWW#");
		crossWord.push_back("###W#W#W###W#W#");
		crossWord.push_back("#WWWWW#WWWWWWW#");
		crossWord.push_back("#W###W#W#W#W#W#");
		crossWord.push_back("#WWWWWWWWW#WWW#");
		crossWord.push_back("#W#W#W#W#W#W#W#");
		crossWord.push_back("#WWWWWWW#WWWWW#");
		crossWord.push_back("###############");
		return crossWord;
}
