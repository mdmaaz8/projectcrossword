#include <iostream>
#include <map>
#include <vector>
#include <string>
#include "headerFile.h"

#define N 20

using namespace std;

    
void doNumbering(vector<string> crossWord, int numberMatrix[N][N]) {
		int length = crossWord.size();
		int rowFlag[N] = {0};
		int colFlag[N] = {0};
		int counter = 1;
		for(int i = 0; i < length; i++) {
				numberMatrix[0][i] = -1;
				numberMatrix[i][0] = -1;
				numberMatrix[length-1][i] = -1;
				numberMatrix[i][length-1] = -1;
		}
		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(isWhite(crossWord[i][j])) {
								if(isWordStartInRow(j, rowFlag[i], crossWord[i]) || isWordStartInCol(j, colFlag[j], crossWord[i+1])) {
										numberMatrix[i][j] = counter++;
										rowFlag[i] = 1;
										colFlag[j] = 1;
								}
						}
						else {
								numberMatrix[i][j] = -1;
								rowFlag[i] = 0;
								colFlag[j] = 0;
						}
				}
		}
}

bool isWhite(char ch) {
		return ch == 'W';
}

bool isWordStartInRow(int index, int rowFlag, string rowString) {
		return rowFlag == 0 && isWhite(rowString[index+1]);
}

bool isWordStartInCol(int index, int colFlag, string nextRowString) {
		return colFlag == 0 && isWhite(nextRowString[index]);
}

void printNumberMatrix(int numberMatrix[N][N], int length) {
		for(int i = 0; i < length-1; i++) {
				for(int j = 0; j < length-1; j++) {
						if(numberMatrix[i][j] == 0)
								cout << "__  ";
						else if(numberMatrix[i][j] == -1)
								cout << "##  ";
						else {
								if(numberMatrix[i][j] < 10)
										cout << " " << numberMatrix[i][j] << "  ";
								else
										cout << numberMatrix[i][j] << "  ";
						}
				}
				cout << endl;
		}
}

vector<string> getInput() {
		vector<string> crossWord;
		crossWord.push_back("###############");
		crossWord.push_back("#WWWWW#WWWWWWW#");
		crossWord.push_back("#W#W#W#W#W#W#W#");
		crossWord.push_back("#WWW#WWWWWWWWW#");
		crossWord.push_back("#W#W#W#W#W###W#");
		crossWord.push_back("#WWWWWWW#WWWWW#");
		crossWord.push_back("#W#W###W#W#W###");
		crossWord.push_back("#WWWWWW#WWWWWW#");
		crossWord.push_back("###W#W#W###W#W#");
		crossWord.push_back("#WWWWW#WWWWWWW#");
		crossWord.push_back("#W###W#W#W#W#W#");
		crossWord.push_back("#WWWWWWWWW#WWW#");
		crossWord.push_back("#W#W#W#W#W#W#W#");
		crossWord.push_back("#WWWWWWW#WWWWW#");
		crossWord.push_back("###############");
		return crossWord;
}
