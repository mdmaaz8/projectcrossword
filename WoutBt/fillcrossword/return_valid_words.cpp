#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include "headerFile.h"

using namespace std;


map<int,vector<string> >genMap(){
	
	map<int,vector<string> >m;
  	string line;
  	ifstream myfile ("./data_source/refined.txt");
  	if (myfile.is_open())
  	{
    	while ( myfile.good() )
    	{
      		getline (myfile,line);
			m[line.size()].push_back(line);
 	  	}
    	myfile.close();
  	}
	else
		cout << "Unable to open file. Check path of refined.txt"; 
	
	return m;
}


vector <PAIR_IC> returnWordConstraint(vector<vector<char> > charMatrix, PAIR_IC p, int numberMatrix[N][N]) {
		int length = charMatrix.size();
		vector <PAIR_IC> wordConstraints;

		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(numberMatrix[i][j] == p.first) {
								if(p.second == 'a'){
										int index = 0;
										int temp = j;
										while(charMatrix[i][temp] != '#'){
												if (charMatrix[i][temp] != '-')
														wordConstraints.push_back(make_pair(index,charMatrix[i][temp]));
												temp++;
												index++;
										}
								}
								else {
										int index = 0;
										int temp = i;
										while(charMatrix[temp][j] != '#'){
												if (charMatrix[temp][j] != '-')
														wordConstraints.push_back(make_pair(index,charMatrix[temp][j]));
												temp++;
												index++;
										}
								}	
								return wordConstraints;
						}

				}
		}

}


vector<string> returnValidWords( map<int, vector<string> > dict, vector< pair<int, char> > pos, int length ) {
    if (pos.empty())
        return dict.find(length)->second;
    vector<string> checkWords = dict.find(length)->second;
    vector<string> validWords;
    bool valid = true;
    for ( int i = 0; i < checkWords.size(); i++ ) {
        for (int j = 0; j < pos.size(); j++) {
            if (checkWords[i][pos[j].first] != pos[j].second)
                valid = false;
        }
        if (valid)
            validWords.push_back(checkWords[i]);
        valid = true;
    }
	return validWords;
}
    
                
map<int,PAIR_II> calcWordsLength(int numberMatrix[N][N], int length) {
		map<int, PAIR_II> wordsLength;
		int across, down;
		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(numberMatrix[i][j] > 0) {
							if(numberMatrix[i][j-1] == -1)			
								across = getWordLengthAcross(i, j, numberMatrix);
							else
								across = 0;
							if(numberMatrix[i-1][j] == -1)
								down = getWordLengthDown(i, j, numberMatrix);
							else 
								down = 0;
							wordsLength[numberMatrix[i][j]] = make_pair(across,down);
						}
				}
		}
		return wordsLength;
}
