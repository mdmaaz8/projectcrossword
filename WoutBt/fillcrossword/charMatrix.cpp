#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include "headerFile.h"

#define N 20

using namespace std;

vector<vector<char> > generateCharMatrix(int numberMatrix[N][N], int length) {
		vector<vector<char> > charMatrix(length, vector<char>(length));
		for(int i = 0; i < length; i++) {
				charMatrix[0][i] = '#';
				charMatrix[i][0] = '#';
				charMatrix[length-1][i] = '#';
				charMatrix[i][length-1] = '#';
		}
		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(numberMatrix[i][j] == -1)
								charMatrix[i][j] = '#';
						if(numberMatrix[i][j] >= 0)
								charMatrix[i][j] = '-';
				}
		}
		return charMatrix;
}
vector<vector<char> > writeToCharMatrix(vector<vector<char> > charMatrix, int x, int y, string str, char direction, int length) {
        if (direction == 'a'){
           int id = 0;
           for ( int i = y; i < y+length; i++){
               charMatrix[x][i] = str[id];
               id++;
           } 
        }
        if (direction == 'd'){
           int id = 0;
           for ( int i = x; i < x+length; i++){
               charMatrix[i][y] = str[id];
               id++;
           } 
        }                

        return charMatrix;
}




int getWordLengthAcross(int i, int j, int numberMatrix[N][N]) {
		int wordLength = 0;
		if (numberMatrix[i][j+1] !=-1){
           while(numberMatrix[i][j] != -1) {
				wordLength++;
				j++;
		  }
       }
		return wordLength;
}

int getWordLengthDown(int i, int j, int numberMatrix[N][N]) {
		int wordLength = 0;
		if (numberMatrix[i+1][j] !=-1){
		   while(numberMatrix[i][j] != -1) {
				wordLength++;
				i++;
		   }
        }
		
		return wordLength;
}


void printCharMatrix(vector<vector<char> > charMatrix, int length){
     for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						cout << charMatrix[i][j] << " ";
				}
				cout << endl;
		}
}


bool hasOccurredBefore(vector<string> used,string str){
	for(vector<string>::iterator it=used.begin();it!=used.end();it++)
		if ((*it) == str)
			return true;
	return false;
}


