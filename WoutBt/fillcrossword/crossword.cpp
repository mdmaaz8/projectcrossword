#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include "headerFile.h"

#define N 20

using namespace std;


int main() {
		int xo;
		vector<string> usedWords;
		map<int, vector<string> > dict = genMap();
		vector<string> crossWord = getInput();
		int numberMatrix[N][N] = {0};
		doNumbering(crossWord, numberMatrix);
		printNumberMatrix(numberMatrix, crossWord.size());
		map<int, PAIR_II> wordsLength = calcWordsLength(numberMatrix, crossWord.size());
		map<int, PAIR_II> :: iterator it;
		map<PAIR_IC, vector<int> > dependencyList;
		dependencyList = createDependencyList(numberMatrix, crossWord.size());
		map<PAIR_IC, vector<int> > :: iterator is;

		vector<vector<char> > charMatrix = generateCharMatrix(numberMatrix, crossWord.size());
		printCharMatrix(charMatrix, crossWord.size());

		for (int i = 1; i < crossWord.size()-1; i++) {
				for(int j =1; j < crossWord.size()-1; j++) {

						if (numberMatrix[i][j] > 0){
								int wordId = numberMatrix[i][j];
								if (numberMatrix[i][j-1] == -1 && numberMatrix[i][j+1] != -1){

										PAIR_IC id = make_pair(wordId,'a');
										vector <PAIR_IC> wordConstraint = returnWordConstraint(charMatrix,id,numberMatrix);
										vector<string> validWords= returnValidWords(dict, wordConstraint, wordsLength[wordId].first);
										if (validWords.size() > 0){
												for (int iInVw = 0; iInVw < validWords.size(); iInVw++){                             
														if(!hasOccurredBefore(usedWords,validWords[iInVw])){
																charMatrix = writeToCharMatrix(charMatrix, i, j,validWords[iInVw] , 'a', wordsLength[wordId].first);
																usedWords.push_back(validWords[iInVw]);
																break;
														}
												}
										}
										else{
												cout << "Need to do back tracking" << endl;
												//charMatrix=backTrack(charMatrix,id,numberMatrix,dependencyList);
										}

										printCharMatrix(charMatrix,crossWord.size());
										cout<<endl;
								}

								if (numberMatrix[i-1][j] == -1 && numberMatrix[i+1][j] != -1){
										PAIR_IC id = make_pair(wordId,'d');
										vector <PAIR_IC> wordConstraint = returnWordConstraint(charMatrix,id,numberMatrix);
										vector<string> validWords = returnValidWords(dict, wordConstraint, wordsLength[wordId].second);
										if (validWords.size() > 0){
												for (int iInVw = 0; iInVw < validWords.size(); iInVw++){                             
														if(!hasOccurredBefore(usedWords,validWords[iInVw]))
														{
																charMatrix = writeToCharMatrix(charMatrix, i, j,validWords[iInVw] , 'd',wordsLength[wordId].second);
																cout << validWords[iInVw] <<wordsLength[wordId].second<<endl;
																usedWords.push_back(validWords[iInVw]);
																cout << validWords[0] <<wordsLength[wordId].second<<endl;
																break;
														}
												}
										}
										else{
												cout << "Need to do back tracking" << endl;
										}
										printCharMatrix(charMatrix,crossWord.size());
										cout<<endl;
								}

						}
				}
		}
		return 0;
}


