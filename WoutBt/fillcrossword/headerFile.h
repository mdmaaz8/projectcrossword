#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <fstream>

#define N 20

using namespace std;
typedef pair<int,int> PAIR_II;
typedef pair<int,char> PAIR_IC;

//function declarations
map<int,vector<string> > genMap();
vector<string> returnValidWords( map<int, vector<string> > dict, vector< pair<int, char> > pos, int length );
void doNumbering(vector<string> crossWord, int numberMatrix[N][N]);
bool isWhite(char ch);
bool isWordStartInRow(int index, int rowFlag, string rowString);
bool isWordStartInCol(int index, int colFlag, string nextRowString);
vector<string> getInput();
map<int,PAIR_II> calcWordsLength(int numberMatrix[N][N], int length);
int getWordLengthAcross(int i, int j, int numberMatrix[N][N]);
int getWordLengthDown(int i, int j, int numberMatrix[N][N]);
vector<vector<char> > generateCharMatrix(int numberMatrix[N][N], int length);
vector <PAIR_IC> returnWordConstraint(vector<vector<char> > charMatrix, PAIR_IC p, int numberMatrix[N][N]);
map<PAIR_IC, vector<int> > createDependencyList(int numberMatrix[N][N], int length );
void printNumberMatrix(int numberMatrix[N][N], int length);
void printCharMatrix(vector<vector<char> >, int);
vector<vector<char> > writeToCharMatrix(vector<vector<char> > charMatrix, int x, int y, string str, char direction, int length);
bool hasOccurredBefore(vector<string> used,string str);
