#include<iostream>
#include<map>
#include<vector>
#include<string>
#include "headerFile.h"

#define N 20

using namespace std;
typedef pair<int,int> PAIR_II;
typedef pair<int,char> PAIR_IC;

map<PAIR_IC, vector<int> > createDependencyList(int numberMatrix[N][N], int length ){
		map<PAIR_IC, vector<int> > dependencyList;
		for(int i = 1; i < length-1; i++) {
				for(int j = 1; j < length-1; j++) {
						if(numberMatrix[i][j] > 0) {
								if(numberMatrix[i][j-1] == -1){
									int l = j;
									vector<int> list;

									list.push_back(-1);
									while(numberMatrix[i][l] != -1){
											int k = i;
											if(numberMatrix[i-1][l] != -1){
												while(numberMatrix[k-1][l] != -1) {
													k--;
												}
												list.push_back(numberMatrix[k][l]);
											}
											l++;
									}

									sort(list.begin(), list.end());
									reverse(list.begin(), list.end());
									dependencyList[make_pair(numberMatrix[i][j], 'a')] = list;
								}
								if(numberMatrix[i-1][j] == -1){
										int l = i;
										vector<int> list;
										list.push_back(-1);
										int k = j;
										if(numberMatrix[l][k-1] != -1){
												while(numberMatrix[l][k-1] != -1) {
														k--;
												}
												list.push_back(numberMatrix[l][k]);
										}
										sort(list.begin(), list.end());
										reverse(list.begin(), list.end());
										dependencyList[make_pair(numberMatrix[i][j],'d')] = list;
								}
						}
				}
		}
		return dependencyList;
}
