#include<iostream>
#include <fstream>
#include <string>
#include<vector>
#include<map>

using namespace std;

map<int,vector<string> > genMap();

int main() {
	map<int,vector<string> > m=genMap();
	for(map<int,vector<string> >::iterator it=m.begin();it!=m.end();it++)
		for(vector<string>::iterator iter=it->second.begin();iter!=it->second.end();iter++)
			cout<<*iter<<endl;	
	return 0;
}

map<int,vector<string> >genMap(){
	
	map<int,vector<string> >m;
  	string line;
  	ifstream myfile ("./data_source/refined.txt");
  	if (myfile.is_open())
  	{
    	while ( myfile.good() )
    	{
      		getline (myfile,line);
			m[line.size()].push_back(line);
 	  	}
    	myfile.close();
  	}
	else
		cout << "Unable to open file. Check path of refined.txt"; 
	
	return m;
}
