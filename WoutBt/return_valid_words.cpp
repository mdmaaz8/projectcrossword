#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <fstream>
//#include "mapcreator.h"

using namespace std;

map<int, vector<string> > genMap();
vector<pair<int,char> > get_char_pos();
vector<string> returnValidWords( map<int, vector<string> > dict, vector< pair<int, char> > pos, int length );

int main() {
    map<int, vector<string> > dict = genMap();
    vector<pair<int, char> > char_pos = get_char_pos();
    vector<string> words = returnValidWords( dict, char_pos, 3);
    for ( int i = 0; i < words.size(); i++ )
        cout << words[i] << endl;
    return 0;
}


map<int,vector<string> >genMap(){
	
	map<int,vector<string> >m;
  	string line;
  	ifstream myfile ("./data_source/refined.txt");
  	if (myfile.is_open())
  	{
    	while ( myfile.good() )
    	{
      		getline (myfile,line);
			m[line.size()].push_back(line);
 	  	}
    	myfile.close();
  	}
	else
		cout << "Unable to open file. Check path of refined.txt"; 
	
	return m;
}

vector<pair<int,char> > get_char_pos() {
    vector<pair<int, char> > char_pos;
    char_pos.push_back(make_pair(0,'a'));
    char_pos.push_back(make_pair(2,'k'));
    return char_pos;
}

vector<string> returnValidWords( map<int, vector<string> > dict, vector< pair<int, char> > pos, int length ) {
    if (pos.empty())
        return dict.find(length)->second;
    vector<string> checkWords = dict.find(length)->second;
    vector<string> validWords;
    bool valid = true;
    for ( int i = 0; i < checkWords.size(); i++ ) {
        for (int j = 0; j < pos.size(); j++) {
            if (checkWords[i][pos[j].first] != pos[j].second)
                valid = false;
        }
        if (valid)
            validWords.push_back(checkWords[i]);
        valid = true;
    }
	return validWords;
}
    
                
